<?php
\Larakit\StaticFiles\Manager::package('larakit/sf-angular-select2')
    ->setSourceDir('public')
    ->usePackage('larakit/sf-angular')
    ->usePackage('larakit/sf-bootstrap')
    ->ngModule('ui.select')
    ->cssPackage('select.min.css')
    ->jsPackage('select.min.js');
